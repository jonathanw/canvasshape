"use strict";
let canvas;
let context;
let rotation = 0;
function init() {
    canvas = document.getElementById('canvas');
    context = canvas.getContext('2d');
    requestAnimationFrame(animate);
}
function animate() {
    // reset transforms before clearing
    context.setTransform(1, 0, 0, 1, 0, 0);
    context.clearRect(0, 0, canvas.width, canvas.height);
    // tramslate and rotate an absolute rotation value
    context.translate(500 * 0.5, 500 * 0.5);
    context.rotate(rotation);
    context.translate(-500 * 0.5, -500 * 0.5);
    // Set line width
    context.lineWidth = 5;
    context.beginPath();
    context.moveTo(100, 400);
    context.lineTo(400, 20);
    context.lineTo(495, 400);
    context.lineTo(240, 100);
    context.lineTo(350, 400);
    context.lineTo(150, 200);
    context.lineTo(200, 410);
    context.closePath();
    context.stroke();
    rotation += -0.0001;
    requestAnimationFrame(animate);
}
init();
